﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.Models
{
    public class Jogo
    {
        private readonly int _id;
        private string _titulo;
        private string _urlCapa;
        private DateTime _dataLacamento;
        private string[] _desenvolvedor;
        private string[] _plataformas;
        bool _excluido;


        public Jogo(int id)
        {
            _id = id;
        }

        public string Titulo {

            get { 
                
                return _titulo; 
            }

            set { 
                _titulo = value; 
            } 
        }

        public string UrlCapa {

            get 
            {
                return _urlCapa;
            }

            set
            {
                _urlCapa = value;
            } 
        }

        public DateTime DataLancamento {

            get { 
                return _dataLacamento; 
            }

            set { 
                _dataLacamento = value; 
            } 
        }

        public string[] Desenvolvedor {
            
            get { 
                return _desenvolvedor; 
            }

            set
            {
                _desenvolvedor = value;
            } 
        }

        public string[] Plataformas {

            get { 
                return _plataformas; 
            }

            set { 
                _plataformas = value; 
            } 
        }

        public int Id {
            
            get { 
                return _id; 
            }
        }

        public bool Excluido
        {
            get { return _excluido; }
            set { _excluido = value; }
        }

    }
}
