﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.DTOs
{
    public class DTOJogo
    {
        public string Titulo { get; set; }

        public string UrlCapa { get; set; }

        public DateTime DataLancamento { get; set; }

        public string[] Desenvolvedor { get; set; }

        public string[] Plataformas { get; set; }

    }
}
