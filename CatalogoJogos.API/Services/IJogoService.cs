﻿using CatalogoJogos.API.DTOs;
using CatalogoJogos.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.Services
{
    public interface IJogoService
    {
        public IEnumerable<Jogo> GetJogos();
        public Jogo GetJogoById(int id);
        public void CadastrarJogo(DTOJogo jogo);
        public void Atualizarjogo(int idJogo, DTOJogo jogo);
        public void ExcluirJogo(int idJogo);
    }
}
