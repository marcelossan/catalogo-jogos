﻿using CatalogoJogos.API.DTOs;
using CatalogoJogos.API.Models;
using CatalogoJogos.API.Repositories.Jogos;
using CatalogoJogos.API.Services.Configuracao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.Services
{
    public class JogoService: IJogoService
    {
        private readonly IRepJogos _repJogo;
        

        public JogoService(IRepJogos repJogos)
        {
            _repJogo = repJogos;
        }

        public void Atualizarjogo(int idJogo, DTOJogo jogo)
        {
            _repJogo.AtualizarJogo(idJogo, jogo);
        }

        public void CadastrarJogo(DTOJogo jogo)
        {
            _repJogo.SalvarJogo(jogo);
        }

        public Jogo GetJogoById(int id)
        {
           return _repJogo.GetJogos()
                    .Where(jogo => jogo.Id == id)
                    .FirstOrDefault();
        }

        public IEnumerable<Jogo> GetJogos()
        {
            return _repJogo.GetJogos();
        }

        public void ExcluirJogo(int idJogo)
        {
            _repJogo.ExcluirJogo(idJogo);
        }
    }
}
