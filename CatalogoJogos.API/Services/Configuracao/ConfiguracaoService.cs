﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.Services.Configuracao
{
    public class ConfiguracaoService : IConfiguracaoService
    {
        private IConfiguration _config;

        public ConfiguracaoService(IConfiguration configuration)
        {
            _config = configuration;
        }

        public string ObtemEnderecoBancoDeDados()
        {

            var enderecoBancoDeDados = _config.GetSection("BancoDeDados:endereco").Value;

            
            return enderecoBancoDeDados;
        }
    }
}
