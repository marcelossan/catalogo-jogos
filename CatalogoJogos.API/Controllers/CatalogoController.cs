﻿using CatalogoJogos.API.DTOs;
using CatalogoJogos.API.Models;
using CatalogoJogos.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;


namespace CatalogoJogos.API.Controllers
{
    public class CatalogoController : Controller
    {
        private readonly IJogoService _jogoService;

        public CatalogoController(IJogoService jogoService)
        {
            _jogoService = jogoService;
        }

        [HttpGet("ListarJogos")]
        public IEnumerable<Jogo> Get()
        {
            return _jogoService.GetJogos();
        }

        [HttpGet("ListarJogo/{idJogo}")]
        public Jogo GetJogosById(int idJogo)
        {
            return _jogoService.GetJogoById(idJogo);
        }

        [HttpPost("CadastrarJogo")]
        public void CadastrarJogo([FromBody] DTOJogo jogo)
        {
            _jogoService.CadastrarJogo(jogo);
        }

        [HttpPut("AtualizarJogo")]
        public void AtualizarJogo([FromHeader] int idJogo, [FromBody] DTOJogo jogo)
        {
            _jogoService.Atualizarjogo(idJogo, jogo);
        }

        [HttpDelete("ExcluirJogo/{idJogo:int}")]
        public void ExcluirJogo(int idJogo)
        {
            _jogoService.ExcluirJogo(idJogo);
        }
    }
}
