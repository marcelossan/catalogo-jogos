﻿using CatalogoJogos.API.DTOs;
using CatalogoJogos.API.Models;
using CatalogoJogos.API.Services.Configuracao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.Repositories.Jogos
{
    public class RepJogos : IRepJogos
    {
        private readonly IConfiguracaoService _configuracaoService1;

        public RepJogos(IConfiguracaoService configuracaoService)
        {
            _configuracaoService1 = configuracaoService;
        }

        public IEnumerable<Jogo> GetJogos()
        {

            var jogos = ObtemTodosOsJogos();

            return jogos.Where(j => !j.Excluido)
                .OrderBy(x => x.Id).ToList();
        }

        private List<Jogo> ObtemTodosOsJogos()
        {

            string pathDbJogos = _configuracaoService1.ObtemEnderecoBancoDeDados();

            IEnumerable<Jogo> jogos = new List<Jogo>();

            using (StreamReader reader = new StreamReader(pathDbJogos))
            {
                string jsonJogos = reader.ReadToEnd();
                jogos = JsonConvert.DeserializeObject<IEnumerable<Jogo>>(jsonJogos);
            }

            return jogos.OrderBy(x => x.Id).ToList();
        }

        public void SalvarJogo(DTOJogo jogo)
        {
            var jogos = ObtemTodosOsJogos();
            var idNovoJogo = jogos.Max(x => x.Id) + 1;

            Jogo novoJogo = new Jogo(idNovoJogo)
            {
                Titulo = jogo.Titulo,
                UrlCapa = jogo.UrlCapa,
                DataLancamento = jogo.DataLancamento,
                Desenvolvedor = jogo.Desenvolvedor,
                Plataformas = jogo.Plataformas
            };

            var listaJogos = jogos.ToList();
            listaJogos.Add(novoJogo);
            CommitChanges(listaJogos);


        }

        public void AtualizarJogo(int idJogo, DTOJogo jogo)
        {
            List<Jogo> listaJogos;

            var jogoParaAtualizar = BuscaJogo(idJogo, out listaJogos);
            listaJogos.RemoveAll(x => x.Id == idJogo);

            jogoParaAtualizar.Titulo = jogo.Titulo;
            jogoParaAtualizar.UrlCapa = jogo.UrlCapa;
            jogoParaAtualizar.DataLancamento = jogo.DataLancamento;
            jogoParaAtualizar.Desenvolvedor = jogo.Desenvolvedor;
            jogoParaAtualizar.Plataformas = jogo.Plataformas;
            
            listaJogos.Add(jogoParaAtualizar);

            CommitChanges(listaJogos);
        }


        public void ExcluirJogo(int idJogo)
        {
            List<Jogo> listaJogos;

            var jogoParaExcluir = BuscaJogo(idJogo, out listaJogos);
            listaJogos.RemoveAll(x => x.Id == idJogo);
            
            jogoParaExcluir.Excluido = true;    
            
            listaJogos.Add(jogoParaExcluir);
            CommitChanges(listaJogos);

        }

        private Jogo BuscaJogo(int idJogo, out List<Jogo> jogos)
        {
            jogos = ObtemTodosOsJogos().ToList();

            var jogo = jogos.Where(j => j.Id == idJogo).FirstOrDefault();

            if (jogo == null || jogo.Excluido)
            {
                throw new Exception("Jogo não encontrado com o id " + idJogo);
            }

            return jogo;
        }

        private void CommitChanges(List<Jogo> jogos)
        {
            string pathDbJogos = _configuracaoService1.ObtemEnderecoBancoDeDados();

            try
            {
                FileStream fileStream = File.Open(pathDbJogos, FileMode.Open);
                fileStream.SetLength(0);
                fileStream.Close();
            }
            catch (Exception e)
            {
                throw new Exception("Falha ao tentar acessar o banco de dados. " + e.Message);
            }


            var jsonJogos = JsonConvert.SerializeObject(jogos.OrderBy(x => x.Id));

            List<string> listString = new List<string>
            {
                jsonJogos
            };

            File.WriteAllLinesAsync(pathDbJogos, listString);
        }

    }
}
