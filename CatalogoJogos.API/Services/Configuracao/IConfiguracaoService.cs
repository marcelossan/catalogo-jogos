﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogoJogos.API.Services.Configuracao
{
    public interface IConfiguracaoService
    {
        public string ObtemEnderecoBancoDeDados();
    }
}
