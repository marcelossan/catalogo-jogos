﻿using CatalogoJogos.API.DTOs;
using CatalogoJogos.API.Models;
using System.Collections.Generic;


namespace CatalogoJogos.API.Repositories.Jogos
{
    public interface IRepJogos
    {
        public IEnumerable<Jogo> GetJogos();

        public void SalvarJogo(DTOJogo jogo);

        public void AtualizarJogo(int idJogo, DTOJogo jogo);

        public void ExcluirJogo(int idJogo);
    }
}
